var todos = require('../models/todos');
var bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports =  (app) => {
	app.get('/',(req, res) => {
		res.redirect('/app');
	});
	app.get('/app', (req, res) => {
		todos.find({}, (err, allTodos)=>  {
			if (err) {throw err;}
			res.render('todos', {todos: allTodos});
		});
	});
	app.post('/app', urlencodedParser, (req, res) => {
		var todo = req.body.todo;
		var newTodo = {
			todo: todo,
			date: Date.now(),
			completed: false
		};
		todos.create(newTodo, (err, newlyCreated) =>  {
			if (err) {throw err;}
			res.redirect('/app');
		});
	});
	app.delete('/app/:id', urlencodedParser, (req, res) => {
		todos.findByIdAndRemove(req.params.id, function (err) {
			if (err) {throw err;}
			res.redirect('/app');
		});
	});
	app.put('/app/:id', urlencodedParser, (req, res) => {
		todos.findByIdAndUpdate(req.params.id, {'completed': true}, function (err) {
			if (err) {throw err;}
			res.redirect('/app');
		});
	});
};
