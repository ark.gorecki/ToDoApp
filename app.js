
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const methodOverride = require('method-override');
const keys = require('./config/key');

const app = express();
const todoController = require('./controllers/todoController');
// const todos = require('./models/todos');

mongoose.connect(keys.mongoURI, {useMongoClient:true});


app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: false}));
app.use(methodOverride('_method'));


app.use(express.static('./public'));


todoController(app);


const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
	console.info('server is running');
});

