var mongoose = require('mongoose')

const todoSchema = new mongoose.Schema({
  todo: String,
  date: { type: Date, default: Date.now },
  completed: { type: Boolean, default: false }
})

module.exports = mongoose.model('Todos', todoSchema)
